# To self host:
1. install [docker](https://docs.docker.com/engine/install/) and [docker-compose](https://docs.docker.com/compose/install/)
2. execute:
   - `git clone git@gitlab.com:keriks/ecaptcha-solver.git`
   - `cd ecaptcha-solver`
   - `docker-compose up`

## Hosting options:
Previously the eSolver was ran on DigitalOcean's cheapest (5$/mo) droplet (VPS) with average calculation time of 0.8s. By using [referral link](https://m.do.co/c/79d06c900d8e) You'll recieve 100$ towards Your new account. 
[![DigitalOcean Referral Badge](https://web-platforms.sfo2.digitaloceanspaces.com/WWW/Badge%202.svg)](https://www.digitalocean.com/?refcode=79d06c900d8e&utm_campaign=Referral_Invite&utm_medium=Referral_Program&utm_source=badge)
**NB!** It's strongly recommended to enable https when deploying!
