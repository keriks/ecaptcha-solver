#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
NAME="$(basename $DIR)"
PID=".${NAME}.pid"
cd $DIR

if [ -f $PID ]; then
	if test `pgrep -F $PID`; then
		echo "PID file exists and pid is ALIVE - Killing"
		pkill -F $PID && sleep 5
	else
		echo "Pid file exists, but pid is dead"
		rm $PID
	fi
fi
#exit
. venv/bin/activate

python web_service.py &
disown -h %1
