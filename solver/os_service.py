"""
    eCaptcha solver.
    Copyright (C) 2022 Eriks K

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import atexit
import json
import os
import socket
import sys
import time
from _thread import start_new_thread
from io import BytesIO
from pathlib import Path
from signal import SIGTERM

from captcha import CaptchaSolver
from utils import sock_receive, sock_send

# NB! Avoid using me! This code needs bug-fixing and fine-tuning!

HOST = os.environ.get("SOLVER_BIND_HOST", "127.0.0.1")
PORT = int(os.environ.get("SOLVER_BIND_PORT", "12000"))


class Daemon:
    """
    A generic daemon class.

    Usage: subclass the Daemon class and override the run() method
    """

    def __init__(self, pidfile, stdin="/dev/null", stdout="/dev/null", stderr="/dev/null"):
        stdin = Path(stdin)
        stdout = Path(stdout)
        stderr = Path(stderr)
        pidfile = Path(pidfile)
        self.stdin = str(stdin if stdin.is_absolute() else stdin.absolute())
        self.stdout = str(stdout if stdout.is_absolute() else stdout.absolute())
        self.stderr = str(stderr if stderr.is_absolute() else stderr.absolute())
        self.pidfile = str(pidfile if pidfile.is_absolute() else pidfile.absolute())

    def daemonize(self):
        """
        do the UNIX double-fork magic, see Stevens' "Advanced
        Programming in the UNIX Environment" for details (ISBN 0201563177)
        http://www.erlenstar.demon.co.uk/unix/faq_2.html#SEC16
        """
        try:
            pid = os.fork()
            if pid > 0:
                # exit first parent
                sys.exit(0)
        except OSError as e:
            sys.stderr.write(f"fork #1 failed: {e.errno:d} ({e.strerror})\n")
            sys.exit(1)

        # decouple from parent environment
        os.chdir("/")
        os.setsid()
        os.umask(0)

        # do second fork
        try:
            pid = os.fork()
            if pid > 0:
                # exit from second parent
                sys.exit(0)
        except OSError as e:
            sys.stderr.write(f"fork #2 failed: {e.errno:d} ({e.strerror})\n")
            sys.exit(1)

        # redirect standard file descriptors
        sys.stdout.flush()
        sys.stderr.flush()
        si = open(self.stdin, "r")
        so = open(self.stdout, "a+")
        se = open(self.stderr, "a+")
        os.dup2(si.fileno(), sys.stdin.fileno())
        os.dup2(so.fileno(), sys.stdout.fileno())
        os.dup2(se.fileno(), sys.stderr.fileno())

        # write pidfile
        atexit.register(self.delpid)
        pid = str(os.getpid())
        with open(self.pidfile, "w+") as pf:
            pf.write(f"{pid}\n")

    def delpid(self):
        os.remove(self.pidfile)

    def start(self):
        """
        Start the daemon
        """
        # Check for a pidfile to see if the daemon already runs
        try:
            with open(self.pidfile, "r") as pf:
                pid = int(pf.read().strip())
        except IOError:
            pid = None

        if pid:
            sys.stderr.write(f"pidfile {self.pidfile} already exist. Daemon already running?\n")
            sys.exit(1)

        # Start the daemon
        self.daemonize()
        self.run()

    def stop(self):
        """
        Stop the daemon
        """
        # Get the pid from the pidfile
        try:
            with open(self.pidfile, "r") as pf:
                pid = int(pf.read().strip())
        except IOError:
            pid = None

        if not pid:
            sys.stderr.write(f"pidfile {self.pidfile} does not exist. Daemon not running?\n")
            return  # not an error in a restart

        # Try killing the daemon process
        try:
            while 1:
                os.kill(pid, SIGTERM)
                time.sleep(0.1)
        except OSError as err:
            err = str(err)
            if err.find("No such process") > 0:
                if os.path.exists(self.pidfile):
                    os.remove(self.pidfile)
            else:
                print(err)
                sys.exit(1)

    def restart(self):
        """
        Restart the daemon
        """
        self.stop()
        self.start()

    def run(self):
        """
        You should override this method when you subclass Daemon. It will be called after the process has been
        daemonized by start() or restart().
        """
        raise NotImplementedError(
            """
        You should override this method when you subclass Daemon. It will be called after the process has been
        daemonized by start() or restart().
        """
        )


class MyDaemon(Daemon):
    socket: socket

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.captcha_solver = CaptchaSolver(".", "work_dir")

    def out(self, msg="", *args, **kwargs):
        msg = str(msg)
        for arg in args:
            msg += f"\n{arg}"
        for key, value in kwargs.items():
            msg += f"\n{key} = {value}"
        with open(self.stdout, "a+") as f:
            f.write(f"{msg}\n")

    def solve_captcha(self, connection: socket.socket):
        try:
            image = sock_receive(connection)

            io_image = BytesIO(image.getvalue())
            result = self.captcha_solver.solve_captcha(img_file=io_image)

            result_file_location = result.pop("output")
            preview_file_location = result.pop("preview")
            sock_send(connection, json.dumps(result).encode("utf8"))

            with open(result_file_location, "rb") as f:
                sock_send(connection, f.read())

            with open(preview_file_location, "rb") as f:
                sock_send(connection, f.read())
        except BrokenPipeError:
            sys.stderr.write("Remote end closed connection before all data was transferred!\n")
        connection.close()

    def stop(self):
        try:
            self.socket.close()
        except:
            pass
        super().stop()

    def run(self):
        self.socket = socket.socket()
        try:
            self.socket.bind((HOST, PORT))
        except socket.error as e:
            sys.stderr.write(f"Unable to bind address {HOST}:{PORT}!  {e.errno:d} ({e.strerror})\n")
            self.stop()
        self.socket.listen(5)

        while True:
            client, address = self.socket.accept()
            start_new_thread(self.solve_captcha, (client,))


if __name__ == "__main__":
    daemon = MyDaemon("daemon-example.pid", stdout="stdout.log", stderr="stderr.error")
    if len(sys.argv) == 2:
        if "start" == sys.argv[1]:
            daemon.start()
        elif "stop" == sys.argv[1]:
            daemon.stop()
        elif "restart" == sys.argv[1]:
            daemon.restart()
        else:
            print("Unknown command")
            sys.exit(2)
        sys.exit(0)
    else:
        print(f"usage: {sys.argv[0]} start|stop|restart")
        sys.exit(2)
