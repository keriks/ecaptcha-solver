"""
    eCaptcha solver.
    Copyright (C) 2022 Eriks K

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


import base64
import hashlib
import logging
import re
import time
from io import BytesIO
from os import PathLike
from pathlib import Path
from typing import Any, Dict, List, Optional, Union

import cv2
import numpy as np
from PIL import Image
from utils import _MAP, MATCH_COLOUR, CenterPoint, _MatchedRegions, _Matches, _MatchIcon, init_icon_map, logger


class CaptchaSolver:
    icon_map: Dict[str, _MAP]
    logger: logging.Logger
    base_dir: Path
    work_dir: Path
    debug: bool

    def __init__(self, base_dir: Union[Path, str, bytes, PathLike], work_dir_name: str, debug: bool = False):
        self.logger = logger
        self.base_dir = Path(base_dir)
        self.work_dir = self.base_dir / work_dir_name
        self.debug = debug

        self.icon_map = init_icon_map(self.base_dir)

    def solve_captcha(
        self, *, base64_src: str = None, img_file: BytesIO = None, img_path: Path = None, _hash: str = None
    ) -> Dict[str, Any]:
        if len([1 for k in (base64_src, img_file, img_path) if k is not None]) != 1:
            raise ValueError("Must use exactly one of the img passing options!")
        result = {}
        if base64_src is not None:
            b64_image = re.search(r"data:image/\w+;base64,(.*)", base64_src)
            if not b64_image:
                logger.warning(f"Received bad image source: {base64_src=}")
                raise ValueError("base64_src must be in format: `data:image/(jpeg|png);base64,base64Data===`")
            img_file = BytesIO(base64.b64decode(b64_image.group(1)))

        if img_path is not None:
            with open(img_path, "rb") as f:
                img_file = BytesIO(f.read())

        if img_file is not None:
            if _hash is None:
                _hash = hashlib.md5(img_file.getvalue()).hexdigest()
            image_path = self.work_dir / _hash
            image_path.mkdir(parents=True, exist_ok=True)
            source_path = image_path / "source.png"

            # Converting to PNG explicitly
            img_file.seek(0)
            with Image.open(img_file) as img:
                img.save(source_path, format="PNG")

            st = time.time()
            result = self._solve_captcha(source_path, image_path)
            et = time.time()
            logger.info(f"Result: {(et-st) * 1000}ms\n")
            result.update(solve_time=int((et - st) * 1000))
            result.update(_hash=_hash)
            # result: Dict[result=List[Tuple[int, int]], icons=Dict[int, str], solve_time=int, _hash=uuid_str]
        return result

    @staticmethod
    def create_blank(width, height, rgb_color=MATCH_COLOUR.WHITE):
        image = np.zeros((height, width, 3), np.uint8)
        color = tuple(reversed(rgb_color))
        image[:] = color
        return image

    @staticmethod
    def intersection_against_list(x0, y0, x1, y1, lst):
        for a0, b0, a1, b1 in lst:
            if (a0 <= x0 <= a1 or a0 <= x1 <= a1) and (b0 <= y0 <= b1 or b0 <= y1 <= b1):
                return True
        return False

    @staticmethod
    def _mark_as_correct(idx: int, regs: _MatchedRegions, rc: CenterPoint, result_captchas: Dict[int, CenterPoint]):
        regs.append((rc.x - 18, rc.y - 18, rc.x + 18, rc.y + 18))
        result_captchas[idx] = rc

    def _check_result_point(
        self, result, matched_regions: _MatchedRegions, width: int, height: int, accuracy: float
    ) -> Optional[CenterPoint]:
        for pt in zip(*np.where(result >= accuracy)[::-1]):
            rc = CenterPoint(int(pt[0] + int(width / 2)), int(pt[1] + int(height / 2)))
            _intersects = self.intersection_against_list(rc.x - 20, rc.y - 20, rc.x + 22, rc.y + 22, matched_regions)
            if 20 <= rc.x <= 380 and 20 <= rc.y <= 180 and not _intersects:
                return rc
        return None

    def _solve_captcha(
        self, source_image_path: Path, work_dir: Path
    ) -> Dict[str, Union[List[Dict[str, int]], str, Dict[int, str]]]:
        """Solve captcha for source image located in `source_image_path`

        :param source_image_path: Location of the source captcha image
        :return: Dict containing result coordinates and location of result file
        """
        working_dir = work_dir
        result_out = work_dir / "result.png"
        preview_out = work_dir / "preview.png"

        img_rgb = cv2.imread(str(source_image_path))[0:230, 0:400]
        img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)[0:200, 0:400]  # Match only in the upper part

        width = 400
        height = 230
        icn_width = 21
        left = 0
        right = 0 + icn_width
        top = height - 30 + 5
        bottom = height - 4

        matched_regions: _MatchedRegions = []
        captcha_icons_to_match: List[_MatchIcon] = []
        result_icons_names: Dict[int, str] = {}
        # Cut out icons to match
        target_preview = self.create_blank(45, 28)
        while True:
            im1 = cv2.cvtColor(img_rgb[top:bottom, left:right], cv2.COLOR_BGR2GRAY)  # (21x21)
            if len(captcha_icons_to_match) > 5 or right > width:
                break
            if not im1.max():
                right += icn_width
                left += icn_width
                continue
            for icn_name, icn in self.icon_map.items():
                if cv2.matchTemplate(im1, icn["icon"], cv2.TM_CCOEFF_NORMED).max() >= 0.99:
                    template1 = (
                        icn.get("sprite") if icn.get("sprite") is not None else cv2.resize(icn["icon"], (26, 26))
                    )
                    icon_match = _MatchIcon(
                        x=left,
                        name=icn_name,
                        mapped=icn,
                        matches=_Matches(
                            light=cv2.matchTemplate(img_gray, template1, cv2.TM_CCOEFF_NORMED),
                            dark=cv2.matchTemplate(img_gray, cv2.bitwise_not(template1), cv2.TM_CCOEFF_NORMED),
                        ),
                    )
                    captcha_icons_to_match.append(icon_match)
                    idx = len(result_icons_names)
                    result_icons_names[idx] = icn_name
                    cv2.rectangle(img_rgb, (left - 2, top - 2), (right + 1, bottom + 1), MATCH_COLOUR[idx], 2, 2)

                    if self.debug:
                        cv2.imwrite(str(working_dir / f"{idx}.png"), im1)
                        cv2.imwrite(str(working_dir / f"t_{idx}_{icn_name}.png"), icn["icon"])

                    tcon = img_rgb[top - 3 : bottom + 4, left - 3 : right + 4]  # 28x28
                    target_preview = cv2.hconcat(
                        [target_preview, self.create_blank(7, 28), tcon, self.create_blank(7, 28)]
                    )
                    break
            else:
                left += 1
                right += 1
                continue
            left += icn_width
            right += icn_width

        icon_count: int = len(captcha_icons_to_match)
        result_captchas: Dict[int, Optional[CenterPoint]] = {i: None for i in range(icon_count)}
        target_preview = cv2.vconcat(
            [
                self.create_blank(300, 14),
                cv2.hconcat([target_preview, self.create_blank(210 - (icon_count * 42) + 45, 28)]),
            ]
        )

        accuracy_threshold = 0.95
        while not all(result_captchas.values()) or accuracy_threshold > 0.5:
            for idx, _ in result_captchas.items():
                if _ is not None:
                    continue
                _icon = captcha_icons_to_match[idx]
                icon_name = _icon["name"]
                result_icons_names[idx] = icon_name
                match: _Matches = _icon["matches"]
                res_to_use = match["light"] if match["light"].max() > match["dark"].max() else match["dark"]
                pt = self._check_result_point(res_to_use, matched_regions, 26, 26, accuracy_threshold)
                if pt:
                    self.logger.debug(f"{idx:>3} {bool(match['light'].max() > match['dark'].max())} {icon_name}")
                    matched_regions.append((pt.x - 20, pt.y - 20, pt.x + 22, pt.y + 22))
                    result_captchas[idx] = pt
            accuracy_threshold -= 0.05

        preview = self.create_blank(45, 42)
        for i in range(icon_count):
            cp = result_captchas[i]
            if cp:
                x0 = cp.x - 20
                x1 = cp.x + 22
                y0 = cp.y - 20
                y1 = cp.y + 22
                cv2.rectangle(img_rgb, (x0 + 1, y0 + 1), (x1 - 2, y1 - 2), MATCH_COLOUR[i], 2, 2)
                preview = cv2.hconcat([preview, img_rgb[y0:y1, x0:x1]])
            else:
                preview = cv2.hconcat([preview, self.create_blank(42, 42)])

        preview = cv2.hconcat([preview, self.create_blank(300 - preview.shape[1], 42)])
        preview = cv2.vconcat([self.create_blank(300, 33), preview, target_preview, self.create_blank(300, 33)])

        # Dump result for use in eRepublik captcha
        cv2.imwrite(str(result_out), img_rgb)
        cv2.imwrite(str(preview_out), preview)
        if self.debug:
            cv2.imwrite(str(working_dir / "grayscale.png"), img_gray)
        result = []
        for p in result_captchas.values():
            try:
                result.append(dict(x=p.x, y=p.y))
            except (TypeError, AttributeError):
                result.append(dict(CenterPoint()._asdict()))
        return dict(result=result, icons=result_icons_names)
