"""
    eCaptcha solver.
    Copyright (C) 2022 Eriks K

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import logging
import struct
from io import BytesIO
from pathlib import Path
from typing import Dict, List, NamedTuple, Tuple, TypedDict

import cv2
import numpy as np

logger = logging.getLogger("eCaptcha")
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")

log_file = Path("debug/logging.log")
log_file.parent.mkdir(exist_ok=True, parents=True)
file_logger = logging.FileHandler(log_file, "w")
file_logger.setLevel(logging.WARNING)
file_logger.setFormatter(formatter)
logger.addHandler(file_logger)

stream_logger = logging.StreamHandler()
stream_logger.setLevel(logging.DEBUG)
stream_logger.setFormatter(formatter)
logger.addHandler(stream_logger)
logger.propagate = False


_MAP = Dict[str, np.ndarray]
BUFFER_SIZE = 16 * 1024


class CenterPoint(NamedTuple):
    x: int = 0
    y: int = 0


_BGR = Tuple[int, int, int]
_MatchedRegions = List[Tuple[int, int, int, int]]


class _Matches(TypedDict):
    light: np.ndarray
    dark: np.ndarray


class _MatchIcon(TypedDict):
    x: int
    name: str
    mapped: _MAP
    matches: _Matches


class NamedColours(NamedTuple):
    RED: _BGR = (0, 0, 255)
    GREEN: _BGR = (0, 255, 0)
    BLUE: _BGR = (255, 0, 0)
    MAGENTA: _BGR = (255, 0, 255)
    CYAN: _BGR = (255, 255, 0)
    GRAY: _BGR = (190, 190, 190)
    YELLOW: _BGR = (0, 255, 255)
    WHITE: _BGR = (255, 255, 255)


MATCH_COLOUR = NamedColours()


def sock_send(connection, content):
    connection.send(struct.pack(">I", len(content)))
    buffer_size = 1024 * 16
    for offset in range(len(content) // buffer_size + 1):
        connection.send(content[offset * buffer_size : (offset + 1) * buffer_size])


def sock_receive(connection) -> BytesIO:
    content_length: int = struct.unpack(">I", connection.recv(4))[0]
    part = connection.recv(BUFFER_SIZE)
    content = part
    while not len(part) < BUFFER_SIZE and len(content) < content_length:
        part = connection.recv(BUFFER_SIZE)
        content += part

    return BytesIO(content)


def receive_data(connection) -> bytes:
    """This method is not receiving all data"""
    ret = b""
    content_length: int = struct.unpack(">I", connection.recv(4))[0]
    rcved = 0
    while rcved < content_length:
        buf = BUFFER_SIZE if BUFFER_SIZE < content_length - rcved else content_length - rcved
        data = connection.recv(buf)
        ret += data
        rcved += buf
    return ret


def init_icon_map(bp) -> Dict[str, _MAP]:
    icon_map = {}
    sprite_location = bp / Path("sprites")
    for _template in sorted(sprite_location.iterdir()):
        if _template.name.endswith("__tmplt.png"):
            template_name = _template.name[:-11]
            template = cv2.imread(str(_template), cv2.COLOR_BGR2BGRA)
            icon_map[template_name] = {"icon": template}
            if (sprite_location / f"{template_name}.png").exists():
                sprite = cv2.imread(str(sprite_location / f"{template_name}.png"), cv2.COLOR_BGR2BGRA)
                icon_map[template_name]["sprite"] = sprite
    return icon_map
