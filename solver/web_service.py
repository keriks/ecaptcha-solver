"""
    eCaptcha solver.
    Copyright (C) 2022 Eriks K

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import json
import logging
import os
import shutil
import sys
import uuid
from http.server import BaseHTTPRequestHandler, HTTPServer
from pathlib import Path

from captcha import CaptchaSolver

HOST = os.environ.get("SOLVER_BIND_HOST", "127.0.0.1")
PORT = int(os.environ.get("SOLVER_BIND_PORT", "12000"))

logger = logging.getLogger("eCaptchaWeb")
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter("[%(asctime)s] %(levelname)s - %(message)s")

log_file = Path("debug/logging.log")
log_file.parent.mkdir(exist_ok=True, parents=True)
file_logger = logging.FileHandler(log_file, "w")
file_logger.setLevel(logging.WARNING)
file_logger.setFormatter(formatter)
logger.addHandler(file_logger)
logger.propagate = False


class StdRedirect:
    def write(self, data):
        with open(log_file, "w") as f:
            f.write(data)

    def flush(self):
        pass


class CaptchaSolvingWebServer(BaseHTTPRequestHandler):
    captcha_solver: CaptchaSolver

    def __init__(self, *args, **kwargs):
        self.captcha_solver = CaptchaSolver(".", "media", False)
        sys.stderr = StdRedirect()
        super().__init__(*args, **kwargs)

    def do_POST(self):
        _ir_id = uuid.uuid4().hex
        logger.debug(f"Starting to process request {_ir_id}")
        try:
            if self.headers.get("content-type") == "application/json":
                request_length = int(self.headers.get("content-length"))
                data = json.loads(self.rfile.read(request_length))
                if "src" not in data:
                    return self.send_error(
                        _ir_id,
                        explain="Must send json request containing key 'src' with base64 encoded captcha image!",
                        code=400,
                    )
                b64_img = data["src"]
                img_hash = data.get("md5")

            else:
                b64_img = ""
                img_hash = None
            try:
                captcha_result = self.captcha_solver.solve_captcha(base64_src=b64_img, _hash=img_hash)
            except Exception as e:
                logger.exception(str(e), exc_info=e)
                return self.send_error(_ir_id)
            self.send_response(200)
            self.send_header("Content-type", "application/json")
            self.end_headers()
            # captcha_result: Dict[result=List[Tuple[int, int]], icons=Dict[int, str], solve_time=int, _hash=uuid_str]
            self.wfile.write(
                bytes(
                    json.dumps(
                        dict(
                            status=True,
                            **captcha_result,
                        )
                    ),
                    "utf-8",
                )
            )
        except Exception as e:
            logger.exception(str(e), exc_info=e)
            self.send_error(_ir_id)

    def do_GET(self):
        _ir_id = uuid.uuid4().hex
        try:
            _, action, md5_hash, kind = self.path.split("/")
        except:
            return self.send_error(_ir_id, code=404)
        if action != "download":
            return self.send_error(_ir_id, code=404)
        fp = self.captcha_solver.work_dir / md5_hash / f"{kind}.png"
        logger.debug(f"Checking for requested file {fp}, it {fp.exists() and fp.is_file()}")
        if not (fp.exists() and fp.is_file()):
            return self.send_error(_ir_id, code=404)
        fs = fp.stat()
        self.send_response(200)
        self.send_header("Content-type", "image/png")
        self.send_header("Content-Length", str(fs.st_size))
        self.send_header("Last-Modified", self.date_time_string(int(fs.st_mtime)))
        self.end_headers()
        with open(fp, "rb") as f:
            shutil.copyfileobj(f, self.wfile)
        fp.unlink(True)
        if not len([(self.captcha_solver.work_dir / md5_hash).iterdir()]):
            (self.captcha_solver.work_dir / md5_hash).unlink(True)

    def send_error(self, _ir_id, code: int = None, explain: str = None, message: str = None):
        code = code or 500
        try:
            longmsg = self.responses[code][1]
        except KeyError:
            longmsg = "???"
        if not code == 404:
            logger.warning(f"[{_ir_id}] Unsuccessful Request response status: {explain=}")
        super().send_error(code, explain=f"[{_ir_id}] {explain or longmsg}")


if __name__ == "__main__":
    logger.warning(f"Captcha server started http://{HOST}:{PORT}")
    pidfile = ".ecaptcha.pid"
    with open(pidfile, "w") as f:
        f.write(str(os.getpid()))
    webServer = HTTPServer((HOST, PORT), CaptchaSolvingWebServer)
    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass
    finally:
        webServer.server_close()
        logger.critical("Server stopped.")
        os.unlink(pidfile)
