"""
    eCaptcha solver.
    Copyright (C) 2022 Eriks K

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import json
import logging
import time
from pathlib import Path

from captcha import CaptchaSolver
from PIL import Image
from utils import logger

logger.setLevel(logging.DEBUG)
logger.setLevel(10)
for handler in logger.handlers:
    handler.setLevel(10)

program_enter = time.time()

BASE_PATH = Path(".")
SRC_PATH = BASE_PATH / "images/src/"
OUT_PATH = "images/results/"

_BROKEN_IDS = []


if __name__ == "__main__":
    captcha_solver = CaptchaSolver(BASE_PATH, OUT_PATH)
    _start_idx = 0
    images_to_open = []
    for idx in [] or _BROKEN_IDS or range(_start_idx, 50):
        _in = SRC_PATH / f"{idx:03d}.png"
        if not _in.is_file():
            continue
        st = time.time()
        result = captcha_solver.solve_captcha(img_path=_in)
        with open(captcha_solver.work_dir / result.get("_hash") / "result.json", "w") as f:
            json.dump(result, f)

        Image._show(Image.open(result.get("output")), title=f"{idx:03d} PREVIEW")

        print(f"#{idx:<3d}  Execution time: {round((time.time() - st) * 1000, 2)}ms\n")

    print(f"Total time: {time.time() - program_enter}")
