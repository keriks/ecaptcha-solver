function displayToastNotification(msg, title = "", level = 'info') {
  if (!title)
    title = level.toUpperCase();
  let toastContainer = document.getElementById('id_toast_container');
  let toast = document.createElement('div');
  toast.className = 'toast';
  toast.dataset.delay = "2000";
  // toast.style.position = 'absolute'
  // toast.style.top = "0";
  // toast.style.right = "0";

  let toastHead = document.createElement('div');
  toastHead.className = 'toast-header';
  let strongTitle = document.createElement('strong');
  strongTitle.classList.add('mr-auto');
  if (level.toLowerCase() !== 'info')
    strongTitle.classList.add(`text-${level}`);
  strongTitle.textContent = title;
  toastHead.appendChild(strongTitle);
  let dismissButton = document.createElement('button');
  dismissButton.type = 'button';
  dismissButton.className = 'ml-2 mb-1 close';
  dismissButton.dataset.dismiss = 'toast';
  let dismissSpan = document.createElement('span');
  dismissSpan.setAttribute('aria-hidden', 'true');
  dismissSpan.innerHTML = '&times;';
  dismissButton.appendChild(dismissSpan);
  toastHead.appendChild(dismissButton);
  toast.appendChild(toastHead);

  let toastBody = document.createElement('div');
  toastBody.className = 'toast-body';
  toastBody.textContent = msg;
  toast.appendChild(toastBody);
  toastContainer.appendChild(toast);

  $(toast).toast('show')

  $(toast).on('hidden.bs.toast', function () {
    toastContainer.removeChild(toast);
  })
}

const sortObjByKey = fieldName => {
  const sortFn = objToSort => objToSort[fieldName];
  return (curVal, nextVal) => {
    return -1 * (((curVal = sortFn(curVal)) > (nextVal = sortFn(nextVal))) - (nextVal > curVal))
  }
};
const toTitleCase = (str) => str.toLowerCase().split(' ').map(function (word) {
  return (word.charAt(0).toUpperCase() + word.slice(1));
}).join(' ');

