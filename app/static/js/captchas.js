function resultActionButtonOnClick(ev) {
    let _btn = ev.target;
    let dict = {
        credentials: 'same-origin',
        method: 'POST',
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({action: _btn.dataset.action, report_id: _btn.dataset.value})
    };
    fetch(`/captcha/wapi/`, dict
    ).then((response) => response.json()).then(function (response) {
        if (response.status) {
            let card = document.getElementById(`id_card_${response.data.uuid}`);
            let cardHeader = card.querySelector('div.card-header');
            let btn_holder = card.querySelector('div.card-body > div > div.btn-group');
            if (_btn.dataset.action === 'report') {
                card.classList.add('border-danger');
                cardHeader.classList.add('text-white');
                cardHeader.classList.add('bg-danger');
                cardHeader.classList.add('font-weight-bold');

                let btnCorrected = document.createElement('button');
                btnCorrected.type = 'button';
                btnCorrected.dataset.action = 'corrected';
                btnCorrected.dataset.value = response.data.uuid;
                btnCorrected.className = "btn btn-outline-success btn_corrected";
                btnCorrected.textContent = 'Correct';
                btnCorrected.onclick = resultActionButtonOnClick;
                btn_holder.appendChild(btnCorrected)
                let btnResolve = document.createElement('button');
                btnResolve.type = 'button';
                btnResolve.dataset.action = 'resolve';
                btnResolve.dataset.value = response.data.uuid;
                btnResolve.className = "btn btn-outline-info btn_resolve";
                btnResolve.textContent = 'Try again';
                btnResolve.onclick = resultActionButtonOnClick;
                btn_holder.appendChild(btnResolve)

                btn_holder.removeChild(btn_holder.querySelector('.btn_report'));
                displayToastNotification(response.message, 'Reported', 'warning')
            } else if (_btn.dataset.action === 'corrected') {
                card.classList.remove('border-danger');
                cardHeader.classList.remove('text-white');
                cardHeader.classList.remove('bg-danger');
                cardHeader.classList.remove('font-weight-bold');

                let btnReport = document.createElement('button');
                btnReport.type = 'button';
                btnReport.dataset.action = 'report';
                btnReport.dataset.value = response.data.uuid;
                btnReport.className = "btn btn-outline-danger btn_report";
                btnReport.textContent = 'Report';
                btnReport.onclick = resultActionButtonOnClick;
                btn_holder.appendChild(btnReport)
                btn_holder.removeChild(btn_holder.querySelector('.btn_corrected'));
                btn_holder.removeChild(btn_holder.querySelector('.btn_resolve'));
                displayToastNotification(response.message, 'Correct', 'success')
            } else if (_btn.dataset.action === 'resolve') {
                let img = card.querySelector('img.result_img_location')
                img.src = response.data.preview ? response.data.preview : response.data.result_file;
                img.parentElement.dataset.result = response.data.result_file;
                img.parentElement.dataset.original = response.data.file;
                displayToastNotification(response.message)
            }
        }

    }).catch(function (ex) {
        alert("Error occurred. Try again later.");
        if (window.console)
            console.log('failed submitting', ex)
    });
}



if (document.getElementById('id_view_captcha')) {
    window.addEventListener('load', function () {
        if (document.getElementsByClassName('btn_report'))
            for (let btn of document.getElementsByClassName('btn_report')) btn.onclick = resultActionButtonOnClick;
        if (document.getElementsByClassName('btn_corrected'))
            for (let btn of document.getElementsByClassName('btn_corrected')) btn.onclick = resultActionButtonOnClick;
        if (document.getElementsByClassName('btn_resolve'))
            for (let btn of document.getElementsByClassName('btn_resolve')) btn.onclick = resultActionButtonOnClick;

        if (document.getElementsByClassName('cls_open_result_modal')) {
            let captchaResultModal = document.getElementById('id_captcha_result_modal');
            for (let btn of document.getElementsByClassName('cls_open_result_modal')) {
                btn.onclick = (ev) => {
                    let a = ev.target.tagName.toUpperCase() === 'A' ? ev.target : ev.target.parentElement;
                    let originalImageUrl = a.dataset.original;
                    let resultImageUrl = a.dataset.result;
                    console.log(ev.target)
                    let imgEl = document.getElementById('id_captcha_result_modal_image');
                    imgEl.src = resultImageUrl
                    let resultLink = document.getElementById('id_captcha_result_modal_link_result');
                    resultLink.href = resultImageUrl;
                    let originalLink = document.getElementById('id_captcha_result_modal_link_original');
                    originalLink.href = originalImageUrl;
                    $(captchaResultModal).modal()
                }
            }

        }
    });
}
