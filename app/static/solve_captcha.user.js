// ==UserScript==
// @name         eCaptcha solver
// @namespace    http://localhost/
// @version      0.6
// @description  Automatically solve eCaptchas
// @updateURL		http://localhost/static/solve_captcha.user.js
// @downloadURL		http://localhost/static/solve_captcha.user.js
// @author       keriks
// @include      https://www.erepublik.com/*
// @grant        none
// ==/UserScript==


/**
/    Usersript for eCaptcha solver.
/    Copyright (C) 2022 Eriks K
/
/    This program is free software: you can redistribute it and/or modify
/    it under the terms of the GNU General Public License as published by
/    the Free Software Foundation, either version 3 of the License, or
/    (at your option) any later version.
/
/    This program is distributed in the hope that it will be useful,
/    but WITHOUT ANY WARRANTY; without even the implied warranty of
/    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/    GNU General Public License for more details.
/
/    You should have received a copy of the GNU General Public License
/    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/

if (top == self) {
    (function() {
        'use strict';
        if (Environment.hasSessionCaptcha) {
            unlockPopup();
            return;
        }
        function serialize(obj) {
            let str = [];
            for (let p in obj)
                if (obj.hasOwnProperty(p)) {
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                }
            return str.join("&");
        }
        function unlockPopup() {
            let url = '/' + erepublik.settings.culture + '/main/sessionUnlockPopup';
            fetch(url, {
                method: 'GET',
            }).then((response) => { getSessionChallenge(); });
        }
        function getSessionChallenge()
        {
            let url = '/' + erepublik.settings.culture + '/main/sessionGetChallenge';
            let payload = {
                _token: SERVER_DATA.csrfToken,
                captchaId: SERVER_DATA.sessionValidation.captchaId,
                env: sessionCaptcha.getEnv()
            };
            let params = '';
            loop(payload, (e, t) => params += "&" + encodeURIComponent(e) + "=" + encodeURIComponent(t));
            fetch(url, {
                method: 'POST',
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                credentials: 'same-origin',
                body: params.substring(1)
            }).then((response) => {return response.json()}).then((response) => {
                getMatrix(response);
            });
        }
        function getMatrix(sessionChallenge) {
            let url = 'https://erep.lv/captcha/api';
            let payload = {
                src: sessionChallenge.src,
                password: 'CaptchaDevAPI',
                citizenId: erepublik.citizen.citizenId
            };
            let params = '';
            loop(payload, (e, t) => params += "&" + encodeURIComponent(e) + "=" + encodeURIComponent(t));
            fetch(url, {
                method: 'POST',
                headers: {'content-type': 'application/x-www-form-urlencoded'},
                body: serialize(payload)
            }).then((response) => { return response.json(); }).then((response) => { sessionUnlock(sessionChallenge, response.result.result); });
        }
        function sessionUnlock(sessionChallenge, resolvedCaptcha) {
            for (let e in resolvedCaptcha) {
                resolvedCaptcha[e]['x'] += Math.floor(Math.random() * 16) - 8,
                    resolvedCaptcha[e]['y'] += Math.floor(Math.random() * 16) - 8
            }
            let url = '/' + erepublik.settings.culture + '/main/sessionUnlock';
            let payload = {
                _token: SERVER_DATA.csrfToken,
                captchaId: SERVER_DATA.sessionValidation.captchaId,
                imageId: sessionChallenge.imageId,
                challengeId: sessionChallenge.challengeId,
                clickMatrix: JSON.stringify(resolvedCaptcha),
                isMobile: 0,
                env: sessionCaptcha.getEnv(),
                src: sessionChallenge.src
            };
            let params = '';
            loop(payload, (e, t) => params += "&" + encodeURIComponent(e) + "=" + encodeURIComponent(t));
            setTimeout(function(){
                fetch(url, {
                    method: 'POST',
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded"
                    },
                    credentials: 'same-origin',
                    body: params.substring(1)
                }).then((response) => {return response.json()}).then((response) => {
                    if (response.verified) { window.location.reload() };
                });
            }, 4000);
        }
        function loop(a, b) {
            for (let c in a) {
                if (a.hasOwnProperty(c) && !1 === b(c, a[c])) break;
            }
        }
    })();
}
