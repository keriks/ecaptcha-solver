"""
    Web Interface for eCaptcha solver.
    Copyright (C) 2022 Eriks K

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import base64
import datetime
import hashlib
import inspect
import io
import json
import logging
import os
import re
import sqlite3
import time
import uuid
from base64 import b64decode
from decimal import Decimal
from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple, TypedDict, Union

import bcrypt
import requests
from flask import Flask, flash, g, has_request_context, jsonify, redirect, render_template, request, send_file, url_for
from werkzeug.middleware.proxy_fix import ProxyFix
from werkzeug.middleware.shared_data import SharedDataMiddleware


class MyJSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, Decimal):
            return float("{:.02f}".format(o))
        elif isinstance(o, datetime.datetime):
            return o.strftime("%F %T")
        elif isinstance(o, datetime.date):
            return o.isoformat()
        return super().default(o)


class RequestFormatter(logging.Formatter):
    def format(self, record):
        if has_request_context():
            record.url = request.url
            record.remote_addr = request.remote_addr
        else:
            record.url = None
            record.remote_addr = None

        return super().format(record)


class _CaptchaStatsDict(TypedDict):
    total: int
    incorrect: int
    icons: List[Dict[str, Union[str, int]]]


env = os.getenv

app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)
app.json_encoder = MyJSONEncoder


app.config.update(
    SECRET_KEY=env("SECRET_KEY", "example CHANGE ME"),
    REMEMBER_COOKIE_SECURE=True,
    SESSION_REFRESH_EACH_REQUEST=True,
    UPLOAD_FOLDER=Path(app.root_path) / "media",
    MAX_CONTENT_LENGTH=16 * 1024 * 1024,
)

app.logger.setLevel(logging.INFO)
app.add_url_rule("/media/<filename>", "media", build_only=True)
app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {"/media": str(app.config["UPLOAD_FOLDER"])}, cache_timeout=3600)

app.jinja_env.trim_blocks = True
app.jinja_env.lstrip_blocks = True

starttime = datetime.datetime.now()
if not os.path.isfile(env("DATABASE_NAME", "sqlite.db")):
    db = sqlite3.connect(
        env("DATABASE_NAME", "sqlite.db"), detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES
    )
    db.execute(
        "CREATE TABLE captchas(id INTEGER PRIMARY KEY AUTOINCREMENT, created timestamp DEFAULT CURRENT_TIMESTAMP, "
        "file TEXT, result_file TEXT, uuid TEXT, result TEXT, reported BOOLEAN, md_hash TEXT, icon0 TEXT, icon1 TEXT, "
        "icon2 TEXT, icon3 TEXT, icon4 TEXT, icon5 TEXT, icon6 TEXT, calc_time TEXT);"
    )
    db.commit()
    db.close()


def get_db():
    if not hasattr(g, "db"):
        g.db = sqlite3.connect(
            env("DATABASE_NAME", "sqlite.db"), detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES
        )
        g.db.row_factory = sqlite3.Row
    return g.db


def commit():
    db = get_db()
    db.commit()


@app.context_processor
def inject_now():
    return {"now": datetime.datetime.utcnow(), "startts": int(starttime.timestamp())}


@app.template_filter("percent_for")
def percent_for(minor, major):
    if isinstance(minor, (int, float)) and isinstance(major, (int, float)):
        return f"{round(minor/major*100, 2)}%"
    else:
        return f"{minor}/{major}%"


@app.template_filter("query_set")
def query_set(s, _k, _v):
    if isinstance(s, bytes):
        query = s.decode("utf-8").split("&")
    elif isinstance(s, str):
        query = s.split("&")
    else:
        return s
    ret = {}
    for pair in query:
        if not pair:
            continue
        k, v = pair.split("=")
        if k not in ret:
            ret[k] = []
        ret[k].append(v)
    ret[_k] = [_v]
    return_pairs = [f"{k}={v1}" for k, v in ret.items() for v1 in v]
    return "&".join(return_pairs)


@app.teardown_appcontext
def close_db(*args, **kwargs):
    if args or kwargs:
        pass
    if hasattr(g, "db"):
        g.db.close()


@app.route("/")
def index():
    return render_template("base.html")


@app.route("/favicon.ico")
def favicon():
    return send_file("static/img/favicon.ico", download_name="favicon.ico")


def d2j(d: Union[Dict[Any, Any], List[Any]]) -> str:
    return json.dumps(d, sort_keys=True, cls=MyJSONEncoder)


def query_log(fn):
    def wrapper(sql, *args, **kwargs):
        stack = inspect.stack()
        frame = stack[1]
        strfr = f"Caller: {frame.filename[len(app.root_path)+1:]}:{frame.lineno} in {frame.function} |"
        t = time.time()
        try:
            cursor = fn(get_db(), sql, *args, **kwargs)
            app.logger.info(f"({time.time()-t:.4f}) {strfr} {sql=} {args=} {kwargs=}")
        except sqlite3.Error as e:
            app.logger.info(f"({time.time()-t:.4f}) {strfr} ERROR: {e=} {sql=} {args=} {kwargs=}")
            raise
        return cursor

    return wrapper


@query_log
def execute(con, sql, *args, **kwargs) -> sqlite3.Cursor:
    return con.execute(sql, args or kwargs)


def _set_captcha_obj_values(entry: Dict[str, Any]) -> Dict[str, Any]:
    entry["result"] = json.loads(entry["result"] or "[]")
    preview_name = (entry["result_file"] or "")[:-4] + ".preview.png"
    entry["preview"] = (
        url_for("media", filename=preview_name) if os.path.isfile(app.config["UPLOAD_FOLDER"] / preview_name) else ""
    )
    entry["file"] = url_for("media", filename=entry["file"])
    entry["result_file"] = url_for("media", filename=entry["result_file"]) if entry["result_file"] else ""
    entry["calc_time"] = Decimal(entry["calc_time"])
    return entry


def get_captcha_stats() -> _CaptchaStatsDict:
    ret = _CaptchaStatsDict(total=0, incorrect=0, icons=[])
    cur = execute("SELECT count(1) c FROM captchas")
    ret["total"] = cur.fetchone()["c"]
    cur = execute("SELECT count(1) c FROM captchas where reported=true")
    ret["incorrect"] = cur.fetchone()["c"]
    sql_icon_count = (
        "select i as icon, sum(c) as cnt from ("
        "select icon0 i, count(*) c from captchas group by i union "
        "select icon1 i, count(*) c from captchas group by i union "
        "select icon2 i, count(*) c from captchas group by i union "
        "select icon3 i, count(*) c from captchas group by i union "
        "select icon4 i, count(*) c from captchas group by i"
        ") ot where i is not null group by icon order by cnt desc, icon"
    )
    cur = execute(sql_icon_count)
    ret["icons"] += cur.fetchall()
    return ret


def _get_filter_query(query_string: str) -> Tuple[str, List[Any]]:
    if query_string:
        filter_by = query_string.split(",")
        _sql_where = "("
        if len(filter_by) > 1:
            in_part = ",".join(["?"] * len(filter_by))
            _sql_where += " or ".join([f"icon{i} in ({in_part})" for i in range(6)])
        else:
            _sql_where += " or ".join([f"icon{i}=?" for i in range(6)])
        _sql_where += ")"
        _sql_args = filter_by * 6
        return _sql_where, _sql_args
    return "", []


def process_captcha_image_source_file(image: io.BytesIO, filetype: str = "png") -> Dict[str, Any]:
    ret = dict(status=False)
    image_md5 = hashlib.md5(image.getvalue()).hexdigest()

    cur = execute("SELECT uuid, result, file, result_file FROM captchas WHERE md_hash = ?", image_md5)
    _res = cur.fetchone()
    if _res:
        result = {k: _res[k] for k in _res.keys()}
        result["result"] = json.loads(result["result"] or "")
        ret.update(status=True, message="Duplicate", result=result)
    else:
        file_uuid = str(uuid.uuid4())
        filename = f"{file_uuid}.{filetype}"
        location = Path(f"captchas/{datetime.date.today().strftime('%Y/%m/%d')}")
        os.makedirs(app.config["UPLOAD_FOLDER"] / location, exist_ok=True)
        location /= filename
        with open(app.config["UPLOAD_FOLDER"] / location, "wb") as f:
            f.write(image.getvalue())
        try:
            args = str(location), file_uuid, image_md5
            execute("INSERT INTO captchas (file, uuid, md_hash) VALUES (?, ?, ?)", *args)
        except Exception as e:
            app.logger.error(e, exc_info=e)
        commit()
        solve_captcha_for_uuid(file_uuid)
        cur = execute("SELECT uuid, result, file, result_file FROM captchas WHERE md_hash = ?", image_md5)
        _r = cur.fetchone()
        result = {k: _r[k] for k in _r.keys()}
        result["result"] = json.loads(result["result"] or "")

        ret.update(status=True, message="OK", result=result)
    return ret


def solve_captcha_for_uuid(file_uuid: str):
    calculation_time = int(time.time())
    cur = execute("SELECT * FROM captchas WHERE uuid=?", file_uuid)
    row = cur.fetchone()
    try:
        basep = app.config["UPLOAD_FOLDER"]
        if row["result_file"]:
            Path(basep / row["result_file"]).unlink(True)
            Path(basep / (row["result_file"][:-3] + "preview.png")).unlink(True)

        with open(basep / row["file"], "rb") as source_image:
            b64_image = base64.b64encode(source_image.read()).decode("UTF-8")
        solver_host = env("SOLVER_HOST", "solver")
        solver_port = env("SOLVER_BIND_PORT", 12000)
        solver_url = f"http://{solver_host}:{solver_port}"
        solver_response = requests.post(solver_url, json=dict(src=f"data:image/png;base64,{b64_image}"))
        if not solver_response.ok:
            flash("Bad image!", "danger")
            f = basep / row["file"]
            f.replace(str(f.absolute()) + "bad_image")
            execute("DELETE FROM captchas WHERE uuid=?", row["uuid"])
            app.logger.error("ResultJson error!", stack_trace=3)
            return
        _res = solver_response.json()
        end_time = _res["solve_time"]
        if not isinstance(row["created"], datetime.datetime):
            row["created"] = datetime.datetime.strptime(row["created"], "")
        _out = basep / Path(f"captchas/{row['created']:%Y/%m/%d}")
        if not _out.exists():
            app.logger.warning(f'Captcha ({row["id"]}) result output directory {_out} does not exist!')
            _out.mkdir(parents=True, exist_ok=True)

        result_image = io.BytesIO(requests.get(f"{solver_url}/download/{_res['_hash']}/result").content)
        result_image_path = _out / f"{file_uuid}_{calculation_time}.png"
        _res["output"] = str(result_image_path.relative_to(basep))
        with open(result_image_path, "wb") as img:
            img.write(result_image.getvalue())
        result_preview = io.BytesIO(requests.get(f"{solver_url}/download/{_res['_hash']}/preview").content)
        result_preview_path = _out / f"{file_uuid}_{calculation_time}.preview.png"
        with open(result_preview_path, "wb") as img:
            img.write(result_preview.getvalue())

        has_empty = any((o["x"] == 0 or o["y"] == 0 for o in _res["result"])) or row["reported"]
        sql_args = [_res["output"], d2j(_res["result"])]
        icon_fields = ", ".join([f"icon{i}=?" for i in range(6)])
        sql_args += [str(_res["icons"].get(str(i))) for i in range(6)]
        sql_args += [has_empty, end_time, row["uuid"]]
        sql = f"UPDATE captchas SET result_file=?, result=?, {icon_fields}, reported=?, calc_time=? WHERE uuid=?"

        execute(sql, *sql_args)
        commit()
    except Exception as e:
        app.logger.error(e, exc_info=e)
        flash("Error while calculating captcha answer!", "danger")


@app.route("/captcha/recheck/")
def captcha_recheck():
    _order_by = "ORDER BY created, reported DESC, id"
    _sql_where = ""
    _sql_args = []
    if request.args.get("filter"):
        filter_where, filter_args = _get_filter_query(request.args.get("filter"))
        _sql_where += filter_where
        _sql_args += filter_args

    if not _sql_where and not _sql_args:
        _sql_where = "1"
    cur = execute(f"SELECT uuid FROM captchas WHERE {_sql_where} {_order_by} LIMIT 6", *_sql_args)
    results = cur.fetchall()
    if not results:
        return redirect(url_for("captcha_recheck"))
    for row in results:
        solve_captcha_for_uuid(row["uuid"])
    uuid_in = f"uuid in ({','.join(['?'] * len(results))})"
    _sql_where = f"({_sql_where}) AND {uuid_in}"
    _sql_args += [r["uuid"] for r in results]
    cur = execute(f"SELECT * FROM captchas WHERE {_sql_where} {_order_by} LIMIT 6", *_sql_args)
    _rows = cur.fetchall()
    rows = [{k: row[k] for k in row.keys()} for row in _rows]
    for row in rows:
        _set_captcha_obj_values(row)
    return render_template("recheck.html", results=rows)


@app.route("/captcha/stats/", methods=["GET"])
def captcha_stats():
    return render_template("stats.html", stats=get_captcha_stats())


@app.route("/captcha/api/", methods=["POST"])
@app.route("/captcha/api", methods=["POST"])
def captcha_json():
    ret = {"status": False}

    if request.is_json:
        data = request.get_json()
    else:
        data = request.form
    b_pwd = b"$2b$10$d4Mg1q3zMYTcq3hMRaKfDejLGR2Fl2D1OGVIKeNC0jm04YsDTN9yW"
    a_pwd = b"$2b$12$QqI/L0bAOQrC/zps9PPPq.5UllccRbZqhmwnXj/vU.OKN7w5KGVWC"
    password = data.get("password", "").encode("utf-8")
    pass_ok = bcrypt.checkpw(password, b_pwd) or bcrypt.checkpw(password, a_pwd)
    if not pass_ok:
        ret.update(message="auth")
    else:
        if data.get("report"):
            execute("UPDATE captchas SET reported=true WHERE uuid=?", data.get("report"))
            commit()
            ret.update(status=True, message="Thanks for the report!", result={"uuid": data.get("report")})
        elif not data.get("src"):
            ret.update(message="src must be base64 encoded image!")
        else:
            src_re = re.search(r"data:image/(\w+);base64,(.*)", data.get("src"))
            if not src_re:
                ret.update(
                    message="src is not in base64 encoded image format, "
                    "eg. `data:image/(jpeg|png);base64,base64Data===`"
                )
            else:
                filetype = src_re.group(1)
                b64_image = src_re.group(2)
                b = io.BytesIO(b64decode(b64_image.encode("utf-8")))

                ret.update(process_captcha_image_source_file(b, filetype))

    response = jsonify(ret)
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


@app.route("/captcha/api/stats/", methods=["GET"])
def captcha_api_stats():
    # player_id = request.authorization.username
    password = request.authorization.password
    if password not in ["From AF With Love!", "CaptchaDevAPI"]:

        ret = {"status": False}
    else:
        stats = get_captcha_stats()
        popularity = []
        icons = {}
        for row in stats["icons"]:
            popularity.append(row["icon"])
            icons.update({row["icon"]: int(row["count"])})

        ret = dict(status=True, data=dict(incidenece=popularity, **stats))

    response = jsonify(ret)
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


@app.route("/captcha/wapi", methods=["GET", "POST"])
@app.route("/captcha/wapi/", methods=["GET", "POST"])
def captcha_web_api():
    if request.is_json:
        data = request.get_json()
    else:
        data = request.form

    action = data.get("action")
    ret = dict(status=False, message="Not enough data", data=None)

    if action in ["report", "corrected", "resolve"] and data.get("report_id"):
        sql_all_by_uuid = "SELECT * FROM captchas WHERE uuid = ?"
        cur = execute(sql_all_by_uuid, data.get("report_id"))
        _res = cur.fetchone()
        captcha = {k: _res[k] for k in _res.keys()}
        captcha = _set_captcha_obj_values(captcha)
        if captcha:
            ret.update(status=True, message="", data=captcha)
            if action == "report":
                execute("UPDATE captchas SET reported = true WHERE id = ?", captcha.get("id"))
                commit()
                captcha["reported"] = True
                ret.update(data=captcha, message="Captcha reported!")
            elif action == "corrected":
                execute("UPDATE captchas SET reported = false WHERE id = ?", captcha.get("id"))
                commit()
                captcha["reported"] = False
                ret.update(data=captcha, message="Captcha marked as correct!")
            elif action == "resolve":
                solve_captcha_for_uuid(captcha["uuid"])
                result = _set_captcha_obj_values(execute(sql_all_by_uuid, captcha["uuid"]).fetchone())
                ret.update(data=result, message="Recalculated solution of the captcha!")
    response = jsonify(ret)
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


@app.route("/captcha/", methods=["GET", "POST"])
@app.route("/captcha/<string:action>/", methods=["GET", "POST"])
def captcha_html(action=""):
    _order_by = " order by id"
    uuids_to_show: List[str] = []
    shown_uuids = request.form.getlist("shown_uuid") or request.args.getlist("result")
    if shown_uuids:
        if len(shown_uuids) > 1:
            _sql_where_uuid = f'uuid in ({",".join(["?"] * len(shown_uuids))})'
        else:
            _sql_where_uuid = "uuid = ?"
        cur = execute(f"SELECT uuid FROM captchas WHERE {_sql_where_uuid} {_order_by}", *shown_uuids)
        uuids_to_show = [r["uuid"] for r in cur.fetchall()]
    if request.method == "GET":
        args = request.args
        try:
            page = int(args.get("page", 1))
            page = page if page > 1 else 1
        except ValueError:
            page = 1
        pagination = {"page": page}
        page -= 1
        result: List[Optional[Dict[str, Any]]] = []
        _sql_where = ""
        _sql_args = []
        if action:
            if action == "all":
                _sql_where = "1"
            elif action == "incorrect":
                _sql_where = "reported = true"
            else:
                _sql_where = "0"

        if args.get("filter"):
            if _sql_where:
                _sql_where += " and "
            filter_where, filter_args = _get_filter_query(args.get("filter"))
            _sql_where += filter_where
            _sql_args += filter_args

        if _sql_where:
            cur = execute(f"SELECT uuid FROM captchas WHERE {_sql_where} {_order_by}", *_sql_args)
            uuids_to_show = [r["uuid"] for r in cur.fetchall()]
        if uuids_to_show:
            if len(uuids_to_show) > 1:
                sql_in = ",".join(["?"] * len(uuids_to_show[page * 30 : page * 30 + 30]))
                sql_where = f"uuid in ({sql_in})"
            else:
                sql_where = "uuid = ?"
            pagination["total"] = len(uuids_to_show)
            pagination.update(pages=pagination["total"] // 30 + bool(pagination["total"] % 30))
            pagination.update(has_prev=pagination["page"] > 1)
            pagination.update(has_next=pagination["page"] < pagination["pages"])

            query = f"SELECT * FROM captchas WHERE {sql_where} {_order_by}"
            _rows = execute(query, *uuids_to_show[page * 30 : page * 30 + 30]).fetchall()
            result = [{k: row[k] for k in row.keys()} for row in _rows]
            for res in result:
                _set_captcha_obj_values(res)
        return render_template("list.html", results=result, pagination=pagination, stats=get_captcha_stats())

    elif request.method == "POST":
        if request.form.get("report"):
            report_uuid = request.form.get("report")
            execute("UPDATE captchas SET reported=true WHERE uuid=?", report_uuid)
            commit()
            flash("Captcha reported!", "warning")
        elif request.form.get("corrected"):
            execute("UPDATE captchas SET reported=false WHERE uuid=?", request.form.get("corrected"))
            commit()
            flash("Captcha marked as correct!", "success")
        elif request.form.get("resolve"):
            cur = execute("SELECT * FROM captchas WHERE reported=true and uuid=?", request.form.get("resolve"))
            _res = cur.fetchone()
            row = {k: _res[k] for k in _res.keys()}
            if row:
                solve_captcha_for_uuid(row["uuid"])
                flash("Redid captcha!", "info")
        else:
            uuids: List[Tuple[str, bool]] = []
            for file in request.files.getlist("file"):
                if not file or not file.filename:
                    return redirect(url_for("captcha_html"))

                image = io.BytesIO(file.stream.read())

                try:
                    process_captcha_image_source_file(image, file.filename.rsplit(".", 1)[-1])
                except Exception as e:
                    app.logger.error(e, exc_info=e)
                    flash("Error occurred, try again!", "danger")
            for file_uuid, exists in uuids:
                if not exists:
                    solve_captcha_for_uuid(file_uuid)
            uuids_to_show = [u for u, _ in uuids]
        if action in ["all", "incorrect"]:
            return redirect(url_for("captcha_html", action=action))
        else:
            return redirect(f"{url_for('captcha_html')}?" f"{'&'.join(['result=%s' % uid for uid in uuids_to_show])}")
    else:
        return redirect(url_for("captcha_html", action=action))


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=5000)
